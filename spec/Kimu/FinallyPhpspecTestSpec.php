<?php

namespace spec\Kimu;

use Kimu\Client;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class FinallyPhpspecTestSpec extends ObjectBehavior
{
    function it_should_throw_an_exception(Client $client)
    {
        $this->setClient($client);
        $client->setUrl('http://foo.com')->shouldBeCalled();
        $client->resetClient()->shouldBeCalled();

        $this->shouldThrow('Exception')->duringSendRequest();
    }
}
