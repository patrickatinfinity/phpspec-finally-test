<?php

namespace Kimu;

class FinallyPhpspecTest
{
    /**
     * @var Client
     */
    private $client;

    public function setClient(Client $client)
    {
        $this->client = $client;
    }

    public function sendRequest()
    {
        try {
            $this->client->setLimit(1);
            $this->client->setUrl('http://foo.com');
            throw new \Exception();
        } finally {
            $this->client->resetClient();
        }
    }
}
